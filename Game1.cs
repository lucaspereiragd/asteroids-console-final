using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using MonoGame.Utilities.Png;

namespace Asteroids
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        static public GraphicsDeviceManager graphics;
        static public SpriteBatch spriteBatch;
        static public Sprite playerSprite;
        static public KeyBoardManager keyboard;
        static public GamePadManager gamePad;
        static public BulletList bullets;

        Alien alien;
        Background background;

        int wave = 0;
        int lives = 3;
        int counter = 0;

        int count = 0;

        SpriteFont font;

        Asteroid asteroids;

        Collision collision;

        private FrameCounter _frameCounter = new FrameCounter();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 800;
            //graphics.IsFullScreen = true;

            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            playerSprite = new Sprite(graphics, Content.Load<Texture2D>("asteroidsShip"));

            alien = new Alien(Content, graphics, playerSprite);
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            background = new Background(graphics.GraphicsDevice, spriteBatch, Content);
            bullets = new BulletList();
            asteroids = new Asteroid(Content, graphics);
            keyboard = new KeyBoardManager(Content);
            gamePad = new GamePadManager(Content);
            collision = new Collision(playerSprite, alien, bullets, asteroids);

            font = Content.Load<SpriteFont>("Font");

            GamePadManager.threadGamePad.Start();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            //Para ver que os controles est�o no gameTime corretamente.
            keyboard.Update(Keyboard.GetState(), graphics, playerSprite, bullets);
            
            playerSprite.Update(gameTime);

            alien.Update(playerSprite, gameTime);

            bullets.Update(gameTime);

            int previousWave = wave;

            wave = asteroids.Update(wave, gameTime);

            if (wave != previousWave)
            {
                counter = 100;
                alien.alienSprite.position = Vector2.Zero;
                alien.LiveAlien();
            }

            lives = collision.Update(graphics, lives);

            base.Update(gameTime);

            stopWatch.Stop();
            if (count <= 60)
                Console.WriteLine($"{stopWatch.Elapsed.Milliseconds}");

            count++;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            
            spriteBatch.Begin();
            background.Draw(spriteBatch);
            spriteBatch.End();


            spriteBatch.Begin();
            bullets.Draw(spriteBatch);
            asteroids.Draw(spriteBatch);
            playerSprite.Draw(spriteBatch);
            alien.Draw(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin();
            for (int i = 0; i < lives; i++)
            {
                Sprite life = new Sprite(graphics, playerSprite.Image);
                life.Rotation = 0;
                life.size = 0.1f;
                life.position = new Vector2(10 + (i * 20), graphics.PreferredBackBufferHeight - 50);
                life.Draw(spriteBatch);
            }
            spriteBatch.End();

            spriteBatch.Begin();
            var deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            _frameCounter.Update(deltaTime);

            var fps = string.Format("FPS: {0}", _frameCounter.AverageFramesPerSecond);

            spriteBatch.DrawString(font, fps, new Vector2(1, 1), Color.Black);
            spriteBatch.End();

            if (counter > 0)
            {
                spriteBatch.Begin();
                spriteBatch.DrawString(font, "Wave " + wave, new Vector2(graphics.PreferredBackBufferWidth / 2 - 60, 200), Color.White * 0.7f);
                spriteBatch.End();
                counter--;
            }
            spriteBatch.Begin();
            if (lives < 0)
            {
                spriteBatch.DrawString(font, "GAME OVER!", new Vector2(200, graphics.PreferredBackBufferHeight / 2), Color.White);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
