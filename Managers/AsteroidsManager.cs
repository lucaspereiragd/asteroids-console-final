﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Asteroids.Managers
{
    class AsteroidsManager
    {
        public static AsteroidsManager instance;
        /// <summary>
        /// É necessário que seja sempre divisível por 4
        /// </summary>
        int numAsteroids = 10000;

        Asteroid[] asteroidThread1;
        Asteroid[] asteroidThread2;
        Asteroid[] asteroidThread3;
        Asteroid[] asteroidThread4;

        Thread thread1;
        Thread thread2;
        Thread thread3;
        Thread thread4;

        public AsteroidsManager()
        {
            if (instance != this)
                instance = this;

            asteroidThread1 = new Asteroid[numAsteroids / 4];
            asteroidThread2 = new Asteroid[numAsteroids / 4];
            asteroidThread3 = new Asteroid[numAsteroids / 4];
            asteroidThread4 = new Asteroid[numAsteroids / 4];

            thread1 = new Thread(CheckThread1);
            thread2 = new Thread(CheckThread2);
            thread3 = new Thread(CheckThread3);
            thread4 = new Thread(CheckThread4);

            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();
        }

        void CheckThread1()
        {
            Game1.wave++;
            for(int i = 0; i < Game1.wave; i++)
            foreach (Sprite s in astroids)
            {
                s.Rotation += 0.05f;
                s.Update(gameTime);
            }
        }
        void CheckThread2()
        {

        }
        void CheckThread3()
        {

        }
        void CheckThread4()
        {

        }
    }
}
