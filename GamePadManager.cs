﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Asteroids
{
    public class GamePadManager
    {
        private static GamePadState _oldGamePadState;
        private static GamePadState _currentGamePadState;
        private static Texture2D bulletImage;

        public static Thread threadGamePad = new Thread(Run);

        private static void Run()
        {
            while(true)
            {
                Update();
            }
        }

        public GamePadManager(ContentManager Content)
        {
            bulletImage = Content.Load<Texture2D>("laserRed");
        }

        public static void Update()
        {
            _currentGamePadState = GamePad.GetState(PlayerIndex.One);



            if (_currentGamePadState.DPad.Left == ButtonState.Pressed)
            {
                Game1.playerSprite.Rotation -= 0.05f;
            }
            if (_currentGamePadState.DPad.Right == ButtonState.Pressed)
            {
                Game1.playerSprite.Rotation += 0.05f;
            }
            if (WasJustPressed(Buttons.RightShoulder))
            {
                Sprite newBullet = new Sprite(Game1.graphics, bulletImage);

                newBullet.velocity = new Vector2((float)Math.Cos(Game1.playerSprite.Rotation - MathHelper.PiOver2),
                                                 (float)Math.Sin(Game1.playerSprite.Rotation - MathHelper.PiOver2)) * 4.0f
                                                 + Game1.playerSprite.velocity;

                newBullet.position = Game1.playerSprite.position + newBullet.velocity * 1.75f;
                newBullet.Rotation = Game1.playerSprite.Rotation;

                Game1.bullets.Add(newBullet);
            }

            if (_currentGamePadState.Buttons.LeftShoulder == ButtonState.Pressed)
            {
                Game1.playerSprite.velocity = new Vector2((float)Math.Cos(Game1.playerSprite.Rotation - MathHelper.PiOver2),
                                             (float)Math.Sin(Game1.playerSprite.Rotation - MathHelper.PiOver2)) / 4.0f
                                             + Game1.playerSprite.velocity;
            }
            _oldGamePadState = _currentGamePadState;
        }

        static bool WasJustPressed(Buttons b)
        {
            return _oldGamePadState.IsButtonUp(b) && _currentGamePadState.IsButtonDown(b);
        }
    }
}
